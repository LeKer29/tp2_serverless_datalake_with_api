# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "s3-job-offer-bucket-kerpuy" {
  bucket = var.s3_user_bucket_name
  force_destroy = true
}
# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "object" {
  bucket = var.s3_user_bucket_name
  key    = "job_offers/raw/lyon_job_offers.csv"
  source = "/home/cdo/tp2_serverless_datalake_with_api/data/lyon_job_offers.csv"

}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-kerpuy.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}