resource "aws_key_pair" "admin" {
  key_name   = var.aws_keypair_name
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC7go8Yv0qjkumoPoe+MREmOtZ8dBY2M2RRFEAxGwb4ShDaeIkAKOSa0upJ+rnIuiWnan/lv7Y1Z2LH7SxaUViG+n4JzEUKV7c1JoNZLflm9o0QhAUPkBl+0se823xQA3kE60oBi6vTHXtBl3ZXM6BUnvSRHHrOx7JN1xuCx5n1UcvFUV+e45i/YTxxU9pwLcJ9fZh890o3qWRs+T6FIH+3yzTFKsY1yKMD+ST7yeFQ0CL+3cmenQC2HeRDB7+nEYsr5HU5FTy05kAuh8dbZGwDvbvSuHGUcZEhAEE+zuvoktWkux0BBkl/s7IPl4UDSHfgSmNZZ3gebFac2Z1NKd8vzFCBj7frk6pbWf1Z3OhRT+Dz3cCTUDoP46eWcX8FkCJIdul9ByJfURBKFGCmjZaTxm6xTPoDDmiPl48WJHhq2lCDT05AXnzL4UhWRF+kVtHH+eGA/91J1L3wPk32Srq9y6kRpWB/+GX5vbCBoBPAE4Dl8KdnW+uUQLm9c9uDO88= cdo@ubuntu"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 5000
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami                  = var.ami_id
  instance_type        = "t2.micro"
  key_name             = var.aws_keypair_name
  iam_instance_profile = aws_iam_instance_profile.ec2_iam_profile.name
  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.admin]
}


resource "aws_iam_role" "role_ec2" {
  name = "role_ec2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    tag-key = var.tag_name
  }
}
resource "aws_iam_instance_profile" "ec2_iam_profile" {
  name = "ec2_profile"
  role = aws_iam_role.role_ec2.name
}

resource "aws_iam_role_policy_attachment" "aws_ec2_role_athena_policy_attachment" {
  role       = aws_iam_role.role_ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonAthenaFullAccess"
}

resource "aws_iam_role_policy_attachment" "aws_ec2_role_s3_policy_attachment" {
  role       = aws_iam_role.role_ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
